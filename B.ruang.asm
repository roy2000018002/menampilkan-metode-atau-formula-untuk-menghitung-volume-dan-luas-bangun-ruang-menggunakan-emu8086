.model small
.code
org 100h

mulai:
    jmp proses 
    aa db "================================================== $"
    a db 10,13,"|| SELAMAT DATANG DI RAJANYA RUMUS BANGUN RUANG || $"
    bb db 10,13, "================================================== $"
    b db 10,13,10,13,"1.Kubus = $"
    c db 10,13,"Volume : s(sisi) x s x s $"
    d db 10,13,"Luas   : 6 x (s x s) $"
    
    e db 10,13,10,13,"2.Balok = $"
    f db 10,13,"Volume : p(panjang)xl(lebar)xt(tinggi) $"
    g db 10,13,"Luas   : 2 x (pl+lt+pt) $"
    
    h db 10,13,10,13,"3.Prisma Segitiga = $"
    i db 10,13,"Volume : Luas alas x t(tinggi) $"
    j db 10,13,"Luas   : Keliling alasxt + 2xluas alas segitiga $"
    
    k db 10,13,10,13,"4.Limas Segiempat = $"
    l db 10,13,"Volume : 1/3 x p x l x t $"
    m db 10,13,"Luas   : Luas alas + luas selubung limas $ "
    
    p db 10,13,10,13,"5.Limas Segitiga = $"
    q db 10,13,"Volume : 1/3 x luas alas x t $"
    r db 10,13,"Luas   : Luas alas + Luas selubung limas $"
    
    x db 10,13,10,13,"6.Tabung = $"
    y db 10,13,"Volume : phi(22/7 dan 3,14) x rxr x t $"
    z db 10,13,"Luas   : (2 x luas alas)+(keliling alas x tinggi) $"
    
    v db 10,13,10,13,"7.Kerucut = $" 
    w db 10,13,"Volume : 1/3 x phi x r2 x t $"
    u db 10,13,"Luas   : ( phi x rxr ) + ( phi x r x s) $"
    
    n db 10,13,10,13,"8.Bola = $"
    o db 10,13,"Volume : 4/3 x phi x rxrxr $"
    s db 10,13,"Luas   : 4 x phi x rxr $"
    
    dd db 10,13,10,13,"SELAMAT BERHITUNG :) $ "
    
    proses:
   
    mov ah, 09h
    lea dx, aa
    int 21h
    mov ah, 09h
    lea dx, a
    int 21h
    mov ah, 09h
    lea dx, bb
    int 21h
       
    mov ah, 09h
    lea dx, b
    int 21h   
    mov ah, 09h
    lea dx, c
    int 21h
    mov ah, 09h
    lea dx, d
    int 21h  
    
    mov ah, 09h
    lea dx, e
    int 21h
    mov ah, 09h
    lea dx, f
    int 21h
    mov ah, 09h
    lea dx, g
    int 21h
    
    mov ah, 09h
    lea dx, h
    int 21h
    mov ah, 09h
    lea dx, i
    int 21h
    mov ah, 09h
    lea dx, j
    int 21h
    
    mov ah, 09h
    lea dx, k
    int 21h
    mov ah, 09h
    lea dx, l
    int 21h
    mov ah, 09h
    lea dx, m
    int 21h
    
    mov ah, 09h
    lea dx, p
    int 21h
    mov ah, 09h
    lea dx, q
    int 21h
    mov ah, 09h
    lea dx, r
    int 21h
    
    mov ah, 09h
    lea dx, x
    int 21h
    mov ah,09h
    lea dx, y
    int 21h
    mov ah, 09h
    lea dx, z
    int 21h
    
    mov ah,09h 
    lea dx, v
    int 21h
    mov ah, 09h
    lea dx, w
    int 21h
    mov ah, 09h
    lea dx, u
    int 21h
    
    mov ah,09h
    lea dx, n
    int 21h
    mov ah, 09h
    lea dx, o
    int 21h
    mov ah, 09h
    lea dx, s
    int 21h 
    
    mov ah, 09h
    lea dx, dd
    int 21h
    int 20h 
    end mulai